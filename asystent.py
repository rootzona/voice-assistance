#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: greg
"""
from datetime import datetime
from logging.config import listen

import speech_recognition as sr
import pyttsx3
import webbrowser
import wikipedia
import wolframalpha
import os

engine = pyttsx3.init()
voices = engine.getProperty('voices')
# 0 et cetera = different voices and human syntax, in my case is 4
engine.setProperty('voice', voices[4].id)
activationWord = 'computer'

brave_open = "brave-browser"
webbrowser.register('brave-browser', None, webbrowser.BackgroundBrowser(brave_open))

firefox_open = '/usr/bin/firefox'
stacer_path = '/usr/bin/stacer'
vlc_path = '/usr/bin/vlc'
pycharm_path = '/usr/local/bin/pycharm'
thunar_path = '/usr/bin/thunar'
terminator_path = '/usr/bin/terminator'
ipscan_path = '/usr/bin/ipscan'
flameshot_path = '/usr/bin/flameshot'


def speak(text, rate=120):
    engine.setProperty('rate', rate)
    engine.say(text)
    engine.runAndWait()


def parseCommand():
    listener = sr.Recognizer()
    print('Czekam na komende')

    with sr.Microphone() as source:
        listener.pause_threshold = 2
        input_speech = listener.listen(source)

    try:
        print('Rozpoznawanie mowy...')
        query1 = listener.recognize_google(input_speech, language='en_gb')
        print(f'The input speech was: {query1}')
    except Exception as exception:
        print('Dej komende')
        speak('Dej komende')
        print(exception)
        return 'None'
    return query1


def search_wikipedia(query2=''):
    searchResults = wikipedia.search(query2)
    if not searchResults:
        print('Brak wynikow')
        return 'No results received'
    try:
        wikiPage = wikipedia.page(searchResults[0])
    except wikipedia.DisambiguationError as error:
        wikiPage = wikipedia.page(error.options[0])
    print(wikiPage.title)
    wikiSummary = str(wikiPage.summary)
    return wikiSummary


if __name__ == '__main__':
    speak('komputer wita i pozdrawia')

    while True:
        query = parseCommand().lower().split()
        try:
            if query[0] == activationWord:
                query.pop(0)

                # list commands
                if query[0] == 'say':
                    if 'hello' in query:
                        speak('witam i o zdrowie pytam')
                    else:
                        query.pop(0)
                        speech = ' '.join(query)
                        speak(speech)

                if query[0] == 'go' and query[1] == 'to':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    webbrowser.get('brave-browser').open_new(query)

                if query[0] == 'open' and query[1] == 'firefox':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(firefox_open)

                if query[0] == 'wikipedia':
                    try:
                        query = ' '.join(query[1:])
                        speak('wiki')
                        speak(search_wikipedia(query))
                    except IndexError:
                        print('Nie kumam')
                        speak('Nie kumam')

                if query[0] == 'open' and query[1] == 'stacer':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(stacer_path)

                if query[0] == 'open' and query[1] == 'vlc':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(vlc_path)

                if query[0] == 'open' and query[1] == 'charm':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(pycharm_path)

                if query[0] == 'open' and query[1] == 'files':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(thunar_path)

                if query[0] == 'open' and query[1] == 'terminator':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(terminator_path)

                if query[0] == 'open' and query[1] == 'scan':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(ipscan_path)

                if query[0] == 'open' and query[1] == 'flame':
                    speak('otwieram')
                    query = ' '.join(query[2:])
                    os.popen(flameshot_path)

                if query[0] == 'exit':
                    speak('na razie')
                    break

        except IndexError:
            speak('No dej, dej mi komende')
